package com.example.tema3.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tema3.Book;
import com.example.tema3.R;
import com.example.tema3.interfaces.ActivitiesFragmentsCommunication;

public class BookFragment extends Fragment {
    public static final String TAG_DETAILS = "TAG_DETAILS";

    private Book book;

    private ActivitiesFragmentsCommunication fragmentCommunication;

    public BookFragment(Book book) {
        this.book = book;
    }

    public static BookFragment newInstance(Book book) {
        Bundle args = new Bundle();

        BookFragment fragment = new BookFragment(book);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_book, container, false);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof ActivitiesFragmentsCommunication) {
            fragmentCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        view.findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                //goToLogin
//            }
//        });

        TextView title;
        TextView author;
        TextView description;

        title = view.findViewById(R.id.text_title);
        author = view.findViewById(R.id.text_author);
        description = view.findViewById(R.id.text_description);
        title.setText(book.getTitle());
        author.setText(book.getAuthor());
        description.setText(book.getDescription());
    }
}
