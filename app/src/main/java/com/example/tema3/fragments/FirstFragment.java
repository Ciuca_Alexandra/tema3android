package com.example.tema3.fragments;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextClock;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tema3.Book;
import com.example.tema3.ListAdapter;
import com.example.tema3.R;
import com.example.tema3.interfaces.ActivitiesFragmentsCommunication;
import com.example.tema3.interfaces.OnBookItemClick;

import java.util.ArrayList;

public class FirstFragment extends Fragment {
    public static final String TAG_BOOK = "TAG_BOOK";

    private ActivitiesFragmentsCommunication fragmentsCommunication;

    ArrayList<Book> books = new ArrayList<>();
    RecyclerView booksList;

    ListAdapter listAdapter = new ListAdapter(books, new OnBookItemClick() {
        @Override
        public void onClick(Book book) {
            if (fragmentsCommunication != null) {
                fragmentsCommunication.onReplaceSecondFragment(book);
            }
        }
    });

    public static FirstFragment newInstance() {
        Bundle args = new Bundle();

        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        booksList = (RecyclerView) view.findViewById(R.id.list);
        booksList.setLayoutManager(linearLayoutManager);


        books.clear();
        books.add(new Book("Maytrei", "Miercea Eliade", "roman"));
        books.add(new Book("Plumb", "George Bacovia", "poezie"));
        books.add(new Book("Basme", "Ioan Slavici", "carte pentru copii"));

        booksList.setAdapter(listAdapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof ActivitiesFragmentsCommunication){
            fragmentsCommunication = (ActivitiesFragmentsCommunication) context;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText editText1 = (EditText) view.findViewById(R.id.edit_title);
                String title = editText1.getText().toString();
                if(TextUtils.isEmpty(title) || title.length() > 100) {
                    editText1.setError("Invalid input");
                    return;
                }
                else{
                    editText1.setError(null);
                }

                EditText editText2 = (EditText) view.findViewById(R.id.edit_author);
                String author = editText2.getText().toString();
                if(TextUtils.isEmpty(author) || author.length() > 100) {
                    editText2.setError("Invalid input");
                    return;
                }
                else{
                    editText2.setError(null);
                }

                EditText editText3 = (EditText) view.findViewById(R.id.edit_description);
                String description = editText3.getText().toString();
                if(TextUtils.isEmpty(description) || description.length() > 1000) {
                    editText3.setError("Invalid input");
                    return;
                }
                else{
                    editText3.setError(null);
                }


                Toast.makeText(getContext(), "Add book", Toast.LENGTH_SHORT).show();

                editText1.setText("");
                editText2.setText("");
                editText3.setText("");



                Book book = new Book(title, author, description);
                books.add(book);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                RecyclerView list = (RecyclerView) view.findViewById(R.id.list);
                list.setLayoutManager(linearLayoutManager);
                list.setAdapter(listAdapter);


            }
        });
    }
}
