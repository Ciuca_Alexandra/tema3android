package com.example.tema3.interfaces;

import com.example.tema3.Book;

public interface ActivitiesFragmentsCommunication {
    void onReplaceFragment(String TAG);
    void onReplaceSecondFragment(Book book);
}
