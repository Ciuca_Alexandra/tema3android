package com.example.tema3.interfaces;

import com.example.tema3.Book;

public interface OnBookItemClick {
    void onClick(Book book);
}
