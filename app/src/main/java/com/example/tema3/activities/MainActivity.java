package com.example.tema3.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.tema3.Book;
import com.example.tema3.R;
import com.example.tema3.fragments.BookFragment;
import com.example.tema3.fragments.FirstFragment;
import com.example.tema3.interfaces.ActivitiesFragmentsCommunication;

import static com.example.tema3.fragments.BookFragment.TAG_DETAILS;
import static com.example.tema3.fragments.FirstFragment.TAG_BOOK;

public class MainActivity extends AppCompatActivity implements ActivitiesFragmentsCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        openFirstFragment();
    }

    private void openFirstFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = FirstFragment.class.getName();

        FragmentTransaction addTransaction = transaction.add(R.id.frame_layout, FirstFragment.newInstance(), tag);
        addTransaction.commit();
    }

    @Override
    public void onReplaceFragment(String TAG) { }

    @Override
    public void onReplaceSecondFragment(Book book) {
        String tag = TAG_DETAILS;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        FragmentTransaction addTransaction = fragmentTransaction.replace(R.id.frame_layout, BookFragment.newInstance(book), tag);

        addTransaction.addToBackStack(tag);

        addTransaction.commit();
    }
}