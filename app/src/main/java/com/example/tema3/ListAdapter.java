package com.example.tema3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tema3.interfaces.OnBookItemClick;

import java.util.ArrayList;

public class ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
   ArrayList<Book> books;
   private OnBookItemClick onBookItemClick;

   public ListAdapter(ArrayList<Book> books, OnBookItemClick onBookItemClick){
       this.books = books;
       this.onBookItemClick = onBookItemClick;
   }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_cell, parent, false);
        ListViewHolder listViewHolder = new ListViewHolder(view);
        return listViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
       if(holder instanceof ListViewHolder){
           Book book = (Book) books.get(position);
           ((ListViewHolder) holder).bind(book);
       }
    }

    @Override
    public int getItemCount() {
        return this.books.size();
    }

    class ListViewHolder extends RecyclerView.ViewHolder{
       private TextView title;
       private TextView author;
       private TextView description;
       private View view;

        ListViewHolder(View view){
            super(view);
            title = view.findViewById(R.id.text_title);
            author = view.findViewById(R.id.text_author);
            description = view.findViewById(R.id.text_description);
            this.view = view;
        }

        void bind(Book book)
        {
            title.setText(book.getTitle());
            author.setText(book.getAuthor());
            description.setText(book.getDescription());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onBookItemClick != null){
                        onBookItemClick.onClick(book);
                    }
                }
            });
        }
    }
}
